package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tino.springboot.backend.apirest.models.dao.IRespuestaPreguntaDao;
import com.tino.springboot.backend.apirest.models.entity.RespuestaPregunta;

@Service
public class RespuestaPreguntaService implements IRespuestaPreguntaService {

	@Autowired
	private IRespuestaPreguntaDao respuestaPreguntaDao;
	
	@Override
	public List<RespuestaPregunta> findAll() {
		return (List<RespuestaPregunta>) respuestaPreguntaDao.findAll();
	}

	@Override
	public RespuestaPregunta findById(Long id) {
		return respuestaPreguntaDao.findById(id).orElse(null);
	}

	@Override
	public RespuestaPregunta save(RespuestaPregunta respuestaPregunta) {
		return respuestaPreguntaDao.save(respuestaPregunta);
	}

	@Override
	public void delete(Long id) {
		respuestaPreguntaDao.deleteById(id);
	}

	@Override
	public List<RespuestaPregunta> findByPreguntaId(Integer preguntaId) {
		  List<RespuestaPregunta> listaToda =	(List<RespuestaPregunta>) respuestaPreguntaDao.findAll();
		  listaToda.removeIf(i -> i.getPreguntaId() != preguntaId);
		  return listaToda;
	}

	@Override
	public List<RespuestaPregunta> findByRespuestaEncuestaId(Integer respuestaEncuestaId) {
		  List<RespuestaPregunta> listaToda =	(List<RespuestaPregunta>) respuestaPreguntaDao.findAll();
		  listaToda.removeIf(i -> i.getRespuestaEncuestaId() != respuestaEncuestaId);
		  return listaToda;
	}

}
