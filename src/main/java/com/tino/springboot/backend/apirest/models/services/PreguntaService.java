package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IPreguntaDao;
import com.tino.springboot.backend.apirest.models.entity.Pregunta;

@Service
public class PreguntaService implements IPreguntaService {

	@Autowired
	private IPreguntaDao preguntaDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Pregunta> findAll() {
		return (List<Pregunta>) preguntaDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Pregunta findById(Long id) {
		return preguntaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Pregunta save(Pregunta pregunta) {
		return preguntaDao.save(pregunta);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		preguntaDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Pregunta> findByEncuestaId(Integer encuestaId) {
		  List<Pregunta> listaToda =	(List<Pregunta>) preguntaDao.findAll();
		  listaToda.removeIf(i -> i.getEncuestaId() != encuestaId);
		  return listaToda;
	}

}
