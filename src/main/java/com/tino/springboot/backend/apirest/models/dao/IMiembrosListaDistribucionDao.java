package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.MiembrosListaDistribucion;

public interface IMiembrosListaDistribucionDao  extends CrudRepository<MiembrosListaDistribucion,Long>  {

}
