package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.ITipoPreguntaDao;
import com.tino.springboot.backend.apirest.models.entity.TipoPregunta;

@Service
public class TipoPreguntaService implements ITipoPreguntaService {

	@Autowired
	private ITipoPreguntaDao tipoPreguntaDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<TipoPregunta> findAll() {
		return (List<TipoPregunta>) tipoPreguntaDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public TipoPregunta findById(Long id) {
		return tipoPreguntaDao.findById(id).orElse(null);
	}

}
