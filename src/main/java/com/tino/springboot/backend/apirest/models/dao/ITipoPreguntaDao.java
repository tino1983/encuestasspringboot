package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.TipoPregunta;

public interface ITipoPreguntaDao extends CrudRepository<TipoPregunta, Long> {

}
