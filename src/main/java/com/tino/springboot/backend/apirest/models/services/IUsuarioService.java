package com.tino.springboot.backend.apirest.models.services;

import java.util.List;


import com.tino.springboot.backend.apirest.models.entity.Usuario;

public interface IUsuarioService {
	
	public List<Usuario> findAll();
	
	public Usuario findById(Long id);
	
	
	public Usuario save(Usuario usuario);
	
	public void delete(Long id);

	public Usuario findByUsername(String username);

}
