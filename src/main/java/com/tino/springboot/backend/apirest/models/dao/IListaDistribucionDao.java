package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.ListaDistribucion;

public interface IListaDistribucionDao extends CrudRepository<ListaDistribucion,Long> {

}
