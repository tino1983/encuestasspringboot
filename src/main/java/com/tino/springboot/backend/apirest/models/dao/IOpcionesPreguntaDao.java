package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.OpcionesPregunta;

public interface IOpcionesPreguntaDao extends CrudRepository<OpcionesPregunta, Long> {

}
