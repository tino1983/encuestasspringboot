package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.RespuestaOpcion;

public interface IRespuestaOpcionDao extends CrudRepository<RespuestaOpcion, Long> {

}
