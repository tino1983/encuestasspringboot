package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.RespuestaEncuesta;

public interface IRespuestaEncuestaService {
	
	public List<RespuestaEncuesta> findAll();
	
	public RespuestaEncuesta findById(Long id);
	
	public RespuestaEncuesta save(RespuestaEncuesta respuestaEncuesta);
	
	public void delete(Long id);
	

	public List<RespuestaEncuesta> findByEncuestaId(Integer encuestaId);

}
