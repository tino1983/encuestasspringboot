package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tipo_pegunta")
public class TipoPregunta implements Serializable {


    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String tipoPregunta;
	
	@Lob
    @Column(name = "imagen_tipo", columnDefinition="BLOB")
	private byte[] imagenTipo;

	
	private String imagenTipoContentType;
	private String htmlTag;
	private String accion;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public String getTipoPregunta() {
		return tipoPregunta;
	}

	public byte[] getImagenTipo() {
		return imagenTipo;
	}

	public String getImagenTipoContentType() {
		return imagenTipoContentType;
	}

	public String getHtmlTag() {
		return htmlTag;
	}

	public String getAccion() {
		return accion;
	}
	
	
}
