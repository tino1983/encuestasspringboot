package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tino.springboot.backend.apirest.models.dao.IRespuestaEncuestaDao;
import com.tino.springboot.backend.apirest.models.entity.RespuestaEncuesta;

@Service
public class RespuestaEncuestaService implements IRespuestaEncuestaService {

	@Autowired
	private IRespuestaEncuestaDao respuestaEncuestaDao;
	
	@Override
	public List<RespuestaEncuesta> findAll() {
		return (List<RespuestaEncuesta>) respuestaEncuestaDao.findAll();
	}

	@Override
	public RespuestaEncuesta findById(Long id) {
		return respuestaEncuestaDao.findById(id).orElse(null);
	}

	@Override
	public RespuestaEncuesta save(RespuestaEncuesta respuestaEncuesta) {
		return respuestaEncuestaDao.save(respuestaEncuesta);
	}

	@Override
	public void delete(Long id) {
		respuestaEncuestaDao.deleteById(id);
	}

	@Override
	public List<RespuestaEncuesta> findByEncuestaId(Integer encuestaId) {
		  List<RespuestaEncuesta> listaToda =	(List<RespuestaEncuesta>) respuestaEncuestaDao.findAll();
		  listaToda.removeIf(i -> i.getEncuestaId() != encuestaId);
		  return listaToda;
	}

}
