package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.Pregunta;

public interface IPreguntaService {
	
	public List<Pregunta> findAll();
	
	public Pregunta findById(Long id);
	
	public Pregunta save(Pregunta pregunta);
	
	public void delete(Long id);
	
	
	public List<Pregunta> findByEncuestaId(Integer encuestaId);

}
