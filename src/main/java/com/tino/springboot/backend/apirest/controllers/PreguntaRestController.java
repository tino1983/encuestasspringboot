package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.Pregunta;
import com.tino.springboot.backend.apirest.models.services.IPreguntaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class PreguntaRestController {
	
	@Autowired
	private IPreguntaService preguntaService;
	
	@GetMapping("/preguntas")
	public List<Pregunta> index() {
		return preguntaService.findAll();
	}
	
	@GetMapping("/preguntas/{id}")
	public Pregunta show(@PathVariable Long id) {
		return preguntaService.findById(id);
	}
	
	@GetMapping("/preguntas/encuestas/{id}")
	public List<Pregunta>  showPreguntasByEncuestaId(@PathVariable Integer id) {
		return preguntaService.findByEncuestaId(id);
	}
	
	@PostMapping("/preguntas")
	@ResponseStatus(HttpStatus.CREATED)
	public Pregunta create(@RequestBody Pregunta pregunta) {
		return preguntaService.save(pregunta);
	}
	
	@PutMapping("/preguntas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Pregunta update(@RequestBody Pregunta pregunta, @PathVariable Long id) {
		
		Pregunta preguntaActual=preguntaService.findById(id);
		
		preguntaActual.setPregunta(pregunta.getPregunta());
		preguntaActual.setOrden(pregunta.getOrden());
		preguntaActual.setExigeRespuesta(pregunta.getExigeRespuesta());
		
		preguntaActual.setEncuestaId(pregunta.getEncuestaId());
		preguntaActual.setTipoPreguntaId(pregunta.getTipoPreguntaId());
		
		return preguntaService.save(preguntaActual);
	}
	
	@DeleteMapping("/preguntas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		preguntaService.delete(id);
	}
	

}
