package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.MiembrosListaDistribucion;



public interface IMiembrosListaDistribucionService {
	
	public List<MiembrosListaDistribucion> findAll();
	
	public MiembrosListaDistribucion findById(Long id);
	
	public MiembrosListaDistribucion save(MiembrosListaDistribucion miembrosListaDistribucion);
	
	public void delete(Long id);

	public List<MiembrosListaDistribucion> findByListaDistribucionId(Integer listaDistribucionId);

}
