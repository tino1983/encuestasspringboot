package com.tino.springboot.backend.apirest.models.services;

import java.util.List;
import com.tino.springboot.backend.apirest.models.entity.ListaDistribucion;


public interface IListaDistribucionService {
	
	public List<ListaDistribucion> findAll();
	
	public ListaDistribucion findById(Long id);
	
	public ListaDistribucion save(ListaDistribucion listaDistribucion);
	
	public void delete(Long id);

	public List<ListaDistribucion> findByUsuarioId(Integer usuarioId);
}
