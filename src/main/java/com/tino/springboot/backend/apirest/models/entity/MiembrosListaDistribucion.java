package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="miembros_lista_distribucion")
public class MiembrosListaDistribucion implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String emailMiembroLista;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String nombreMiembroLista;
	
	private Boolean esUsuario;
	
	private Integer listaDistribucionId;
	
	private Integer usuarioId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailMiembroLista() {
		return emailMiembroLista;
	}

	public void setEmailMiembroLista(String emailMiembroLista) {
		this.emailMiembroLista = emailMiembroLista;
	}

	public String getNombreMiembroLista() {
		return nombreMiembroLista;
	}

	public void setNombreMiembroLista(String nombreMiembroLista) {
		this.nombreMiembroLista = nombreMiembroLista;
	}

	public Boolean getEsUsuario() {
		return esUsuario;
	}

	public void setEsUsuario(Boolean esUsuario) {
		this.esUsuario = esUsuario;
	}

	public Integer getListaDistribucionId() {
		return listaDistribucionId;
	}

	public void setListaDistribucionId(Integer listaDistribucionId) {
		this.listaDistribucionId = listaDistribucionId;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
