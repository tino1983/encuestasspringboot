package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.EstadoEncuesta;


public interface IEstadoEncuestaDao extends CrudRepository<EstadoEncuesta,Long> {

}
