package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="respuesta_opcion")
public class RespuestaOpcion  implements Serializable {
	

    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private Double valorRespuestaOpcion;
	private String textoRespuestaOpcion;
	
    private Integer opcionesPreguntaId;
    private Integer respuestaPreguntaId;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValorRespuestaOpcion() {
		return valorRespuestaOpcion;
	}

	public void setValorRespuestaOpcion(Double valorRespuestaOpcion) {
		this.valorRespuestaOpcion = valorRespuestaOpcion;
	}

	public String getTextoRespuestaOpcion() {
		return textoRespuestaOpcion;
	}

	public void setTextoRespuestaOpcion(String textoRespuestaOpcion) {
		this.textoRespuestaOpcion = textoRespuestaOpcion;
	}

	public Integer getOpcionesPreguntaId() {
		return opcionesPreguntaId;
	}

	public void setOpcionesPreguntaId(Integer opcionesPreguntaId) {
		this.opcionesPreguntaId = opcionesPreguntaId;
	}

	public Integer getRespuestaPreguntaId() {
		return respuestaPreguntaId;
	}

	public void setRespuestaPreguntaId(Integer respuestaPreguntaId) {
		this.respuestaPreguntaId = respuestaPreguntaId;
	}
	
}
