package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.Encuesta;

public interface IEncuestaService {
	
	public List<Encuesta> findAll();
	
	public Encuesta findById(Long id);
	
	public Encuesta save(Encuesta encuesta);
	
	public void delete(Long id);

}
