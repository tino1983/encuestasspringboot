package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.RespuestaPregunta;
import com.tino.springboot.backend.apirest.models.services.IRespuestaPreguntaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class RespuestaPreguntaRestController {
	
	@Autowired
	private IRespuestaPreguntaService respuestaPreguntaService;
	
	
	@GetMapping("/respuestasPregunta")
	public List<RespuestaPregunta> index() {
		return respuestaPreguntaService.findAll();
	}
	
	@GetMapping("/respuestasPregunta/{id}")
	public RespuestaPregunta show(@PathVariable Long id) {
		return respuestaPreguntaService.findById(id);
	}
	
	@GetMapping("/respuestasPregunta/preguntas/{id}")
	public List<RespuestaPregunta>  showRespuestasPreguntasByPreguntaId(@PathVariable Integer id) {
		return respuestaPreguntaService.findByPreguntaId(id);
	}
	
	@GetMapping("/respuestasPregunta/respuestasEncuesta/{id}")
	public List<RespuestaPregunta>  showRespuestasPreguntasByRespuestaEncuestaId(@PathVariable Integer id) {
		return respuestaPreguntaService.findByRespuestaEncuestaId(id);
	}
	
	@PostMapping("/respuestasPregunta")
	@ResponseStatus(HttpStatus.CREATED)
	public RespuestaPregunta create(@RequestBody RespuestaPregunta respuestaPregunta) {
		return respuestaPreguntaService.save(respuestaPregunta);
	}
	
	@PutMapping("/respuestasPregunta/{id}")
	@ResponseStatus(HttpStatus.OK)
	public RespuestaPregunta update(@RequestBody RespuestaPregunta respuestaPregunta, @PathVariable Long id) {
		
		RespuestaPregunta respuestaPreguntaActual=respuestaPreguntaService.findById(id);
		
		respuestaPreguntaActual.setPreguntaId(respuestaPregunta.getPreguntaId());
		respuestaPreguntaActual.setRespuestaEncuestaId(respuestaPregunta.getRespuestaEncuestaId());
		
		return respuestaPreguntaService.save(respuestaPreguntaActual);
	}
	
	@DeleteMapping("/respuestasPregunta/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		respuestaPreguntaService.delete(id);
	}

}
