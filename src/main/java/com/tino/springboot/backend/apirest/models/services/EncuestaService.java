package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IEncuestaDao;
import com.tino.springboot.backend.apirest.models.entity.Encuesta;

@Service
public class EncuestaService implements IEncuestaService {
	
	@Autowired
	private IEncuestaDao encuestaDao;

	@Override
	@Transactional(readOnly=true)
	public List<Encuesta> findAll() {
	
		return (List<Encuesta>) encuestaDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Encuesta findById(Long id) {
		
		return encuestaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Encuesta save(Encuesta encuesta) {
		
		return encuestaDao.save(encuesta);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		
		encuestaDao.deleteById(id);
	}
	
	

}
