package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.OpcionesPregunta;

public interface IOpcionesPreguntaService {
	
	public List<OpcionesPregunta> findAll();
	
	public OpcionesPregunta findById(Long id);
	
	public OpcionesPregunta save(OpcionesPregunta opcionesPregunta);
	
	public void delete(Long id);
	
	
	public List<OpcionesPregunta> findByPreguntaId(Integer preguntaId);

}
