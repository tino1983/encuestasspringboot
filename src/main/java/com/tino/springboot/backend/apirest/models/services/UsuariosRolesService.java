package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IUsuariosRolesDao;
import com.tino.springboot.backend.apirest.models.entity.UsuariosRoles;

@Service
public class UsuariosRolesService  implements IUsuariosRolesService {

	@Autowired
	private IUsuariosRolesDao usuariosRolesDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<UsuariosRoles> findAll() {
		return (List<UsuariosRoles>) usuariosRolesDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public UsuariosRoles findById(Long id) {
		return usuariosRolesDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public UsuariosRoles save(UsuariosRoles usuariosRoles) {
		return usuariosRolesDao.save(usuariosRoles);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		usuariosRolesDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<UsuariosRoles> findByUserId(Long userId) {
		List<UsuariosRoles> listaToda =	(List<UsuariosRoles>) usuariosRolesDao.findAll();
		listaToda.removeIf(i -> i.getUsuarioId() != userId);
		return listaToda;
	}

}
