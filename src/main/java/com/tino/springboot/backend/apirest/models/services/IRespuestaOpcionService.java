package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.RespuestaOpcion;

public interface IRespuestaOpcionService {
	
	public List<RespuestaOpcion> findAll();
	
	public RespuestaOpcion findById(Long id);
	
	public RespuestaOpcion save(RespuestaOpcion respuestaOpcion);
	
	public void delete(Long id);
	

	public List<RespuestaOpcion> findByOpcionesPreguntaId(Integer opcionesPreguntaId);
	public List<RespuestaOpcion> findByRespuestaPreguntaId(Integer respuestaPreguntaId);

}
