package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="respuesta_pregunta")
public class RespuestaPregunta   implements Serializable  {
	
    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

    private Integer preguntaId;
    private Integer respuestaEncuestaId;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPreguntaId() {
		return preguntaId;
	}

	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	public Integer getRespuestaEncuestaId() {
		return respuestaEncuestaId;
	}

	public void setRespuestaEncuestaId(Integer respuestaEncuestaId) {
		this.respuestaEncuestaId = respuestaEncuestaId;
	}
	

}
