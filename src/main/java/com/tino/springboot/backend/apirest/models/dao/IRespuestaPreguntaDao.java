package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.RespuestaPregunta;

public interface IRespuestaPreguntaDao extends CrudRepository<RespuestaPregunta, Long> {

}
