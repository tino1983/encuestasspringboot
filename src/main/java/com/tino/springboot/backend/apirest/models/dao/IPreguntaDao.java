package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.Pregunta;

public interface IPreguntaDao  extends CrudRepository<Pregunta,Long>  {

}
