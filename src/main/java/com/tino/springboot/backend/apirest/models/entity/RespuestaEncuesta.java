package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="respuesta_encuesta")
public class RespuestaEncuesta   implements Serializable  {
	
    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String ip;
	private String mac;
	private Boolean esUsuario;
	
	private Integer usuarioId;
    private Integer encuestaId;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Boolean getEsUsuario() {
		return esUsuario;
	}

	public void setEsUsuario(Boolean esUsuario) {
		this.esUsuario = esUsuario;
	}

	public Integer getEncuestaId() {
		return encuestaId;
	}

	public void setEncuestaId(Integer encuestaId) {
		this.encuestaId = encuestaId;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

}
