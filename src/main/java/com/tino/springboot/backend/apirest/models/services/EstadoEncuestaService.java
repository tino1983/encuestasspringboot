package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IEstadoEncuestaDao;
import com.tino.springboot.backend.apirest.models.entity.EstadoEncuesta;

@Service
public class EstadoEncuestaService implements IEstadoEncuestaService {

	@Autowired
	private IEstadoEncuestaDao estadoEncuestaDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<EstadoEncuesta> findAll() {
		return (List<EstadoEncuesta>) estadoEncuestaDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public EstadoEncuesta findById(Long id) {
		return estadoEncuestaDao.findById(id).orElse(null);
	}

}
