package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="opciones_pregunta")
public class OpcionesPregunta  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String textoOpcion;
	
	private Double orden;	
	private Double valorMin;
	private Double valorMax;
	private Double step;
	
	private Integer preguntaId;

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public Long getId() {
		return id;
	}


	public String getTextoOpcion() {
		return textoOpcion;
	}


	public void setTextoOpcion(String textoOpcion) {
		this.textoOpcion = textoOpcion;
	}


	public Double getOrden() {
		return orden;
	}


	public void setOrden(Double orden) {
		this.orden = orden;
	}


	public Double getValorMin() {
		return valorMin;
	}


	public void setValorMin(Double valorMin) {
		this.valorMin = valorMin;
	}


	public Double getValorMax() {
		return valorMax;
	}


	public void setValorMax(Double valorMax) {
		this.valorMax = valorMax;
	}


	public Double getStep() {
		return step;
	}


	public void setStep(Double step) {
		this.step = step;
	}


	public Integer getPreguntaId() {
		return preguntaId;
	}


	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
}
