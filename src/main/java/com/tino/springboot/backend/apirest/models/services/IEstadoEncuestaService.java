package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.EstadoEncuesta;



public interface IEstadoEncuestaService {

	public List<EstadoEncuesta> findAll();
	
	public EstadoEncuesta findById(Long id);
}
