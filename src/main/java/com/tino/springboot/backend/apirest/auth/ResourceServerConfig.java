package com.tino.springboot.backend.apirest.auth;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(HttpMethod.GET
				, "/api/encuestas", "/api/encuestas/**"
				,"/api/usuarios", "/api/estadosEncuesta"
				, "/api/tiposPregunta", "/api/preguntas"
				, "/api/opcionesPregunta",  "/api/opcionesPregunta/preguntas/", "/api/respuestasEncuesta"
				, "/api/respuestasPregunta", "/api/respuestasOpcion").permitAll()
		/*.antMatchers(HttpMethod.GET, "/api/encuestas/{id}").hasAnyRole("USER", "ADMIN")
		.antMatchers(HttpMethod.POST, "/api/encuestas/upload").hasAnyRole("USER", "ADMIN")		
		.antMatchers(HttpMethod.POST, "/api/encuestas").hasRole("ADMIN")*/
		.antMatchers(HttpMethod.PUT, "/api/usuarios/**").hasAnyRole("USER", "ADMIN")
		.antMatchers("/v2/api-docs","/swagger-ui.html").permitAll()
		//.antMatchers("/api/encuestas/**").hasAnyRole("ADMIN", "USER")
		.antMatchers(HttpMethod.POST, "/api/usuarios"
				, "/api/respuestasEncuesta", "/api/respuestasPregunta", "/api/respuestasOpcion").permitAll()
		//.antMatchers(HttpMethod.GET, "/api/**").authenticated()
		.antMatchers(HttpMethod.DELETE, "/api/**").authenticated()
		.antMatchers(HttpMethod.PUT, "/api/**").authenticated()
		//.antMatchers(HttpMethod.PUT, "/api/usuarios/**").authenticated()
		.antMatchers(HttpMethod.POST, "/api/**").authenticated()
		/*.anyRequest().authenticated()*/
		.and().cors().configurationSource(corsConfigurationSource());
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowedOrigins(Arrays.asList("http://localhost:4200","https://cev-equipoc.web.app", "https://voxmetric.000webhostapp.com"));
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
		config.setAllowCredentials(true);
		config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
		
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter(){
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

	

}
