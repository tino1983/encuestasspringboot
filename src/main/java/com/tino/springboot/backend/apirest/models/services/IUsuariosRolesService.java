package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.UsuariosRoles;

public interface IUsuariosRolesService {
	
	public List<UsuariosRoles> findAll();
	
	public UsuariosRoles findById(Long id);
		
	public UsuariosRoles save(UsuariosRoles usuariosRoles);
	
	public void delete(Long id);

	public List<UsuariosRoles> findByUserId(Long userId);


}
