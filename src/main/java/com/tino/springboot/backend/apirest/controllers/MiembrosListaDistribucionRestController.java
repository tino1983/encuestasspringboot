package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.MiembrosListaDistribucion;
import com.tino.springboot.backend.apirest.models.services.IMiembrosListaDistribucionService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class MiembrosListaDistribucionRestController {
	
	@Autowired
	private IMiembrosListaDistribucionService miembroslistaDistribucionService;
	
	@GetMapping("/miembrosListaDistribucion")
	public List<MiembrosListaDistribucion> index() {
		return miembroslistaDistribucionService.findAll();
	}
	
	@GetMapping("/miembrosListaDistribucion/{id}")
	public MiembrosListaDistribucion show(@PathVariable Long id) {
		return miembroslistaDistribucionService.findById(id);
	}
	
	@GetMapping("/miembrosListaDistribucion/listasDistribucion/{id}")
	public List<MiembrosListaDistribucion>  showMiembrosByListaDistribucionId(@PathVariable Integer id) {
		return miembroslistaDistribucionService.findByListaDistribucionId(id);
	}
	
	@PostMapping("/miembrosListaDistribucion")
	@ResponseStatus(HttpStatus.CREATED)
	public MiembrosListaDistribucion create(@RequestBody MiembrosListaDistribucion miembrolistaDistribucion) {
		return miembroslistaDistribucionService.save(miembrolistaDistribucion);
	}
	
	@PutMapping("/miembrosListaDistribucion/{id}")
	@ResponseStatus(HttpStatus.OK)
	public MiembrosListaDistribucion update(@RequestBody MiembrosListaDistribucion miembrolistaDistribucion, @PathVariable Long id) {
		
		MiembrosListaDistribucion miembrolistaDistribucionActual=miembroslistaDistribucionService.findById(id);
		
		miembrolistaDistribucionActual.setEmailMiembroLista(miembrolistaDistribucion.getEmailMiembroLista());
		miembrolistaDistribucionActual.setNombreMiembroLista(miembrolistaDistribucion.getNombreMiembroLista());
		miembrolistaDistribucionActual.setEsUsuario(miembrolistaDistribucion.getEsUsuario());
		
		miembrolistaDistribucionActual.setListaDistribucionId(miembrolistaDistribucion.getListaDistribucionId());
		miembrolistaDistribucionActual.setUsuarioId(miembrolistaDistribucion.getUsuarioId());
		
		return miembroslistaDistribucionService.save(miembrolistaDistribucionActual);
	}
	
	@DeleteMapping("/miembrosListaDistribucion/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		miembroslistaDistribucionService.delete(id);
	}
	

}
