package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.TipoPregunta;
import com.tino.springboot.backend.apirest.models.services.ITipoPreguntaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class TipoPreguntaRestController {
	
	@Autowired
	private ITipoPreguntaService tipoPreguntaService;
	
	@GetMapping("/tiposPregunta")
	public List<TipoPregunta> index() {
		return tipoPreguntaService.findAll();
	}
	
	@GetMapping("/tiposPregunta/{id}")
	public TipoPregunta show(@PathVariable Long id) {
		return tipoPreguntaService.findById(id);
	}

}
