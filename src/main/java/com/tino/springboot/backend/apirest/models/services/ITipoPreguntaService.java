package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.TipoPregunta;

public interface ITipoPreguntaService {
	
	public List<TipoPregunta> findAll();
	
	public TipoPregunta findById(Long id);

}
