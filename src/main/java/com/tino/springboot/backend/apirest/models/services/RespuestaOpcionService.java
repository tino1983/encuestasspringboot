package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tino.springboot.backend.apirest.models.dao.IRespuestaOpcionDao;
import com.tino.springboot.backend.apirest.models.entity.RespuestaOpcion;

@Service
public class RespuestaOpcionService implements IRespuestaOpcionService {

	@Autowired
	private IRespuestaOpcionDao respuestaOpcionDao;
	
	@Override
	public List<RespuestaOpcion> findAll() {
		return (List<RespuestaOpcion>) respuestaOpcionDao.findAll();
	}

	@Override
	public RespuestaOpcion findById(Long id) {
		return respuestaOpcionDao.findById(id).orElse(null);
	}

	@Override
	public RespuestaOpcion save(RespuestaOpcion respuestaOpcion) {
		return respuestaOpcionDao.save(respuestaOpcion);
	}

	@Override
	public void delete(Long id) {
		respuestaOpcionDao.deleteById(id);
	}

	@Override
	public List<RespuestaOpcion> findByOpcionesPreguntaId(Integer opcionesPreguntaId) {
		  List<RespuestaOpcion> listaToda =	(List<RespuestaOpcion>) respuestaOpcionDao.findAll();
		  listaToda.removeIf(i -> i.getOpcionesPreguntaId() != opcionesPreguntaId);
		  return listaToda;
	}

	@Override
	public List<RespuestaOpcion> findByRespuestaPreguntaId(Integer respuestaPreguntaId) {
		  List<RespuestaOpcion> listaToda =	(List<RespuestaOpcion>) respuestaOpcionDao.findAll();
		  listaToda.removeIf(i -> i.getRespuestaPreguntaId() != respuestaPreguntaId);
		  return listaToda;
	}

}
