package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tino.springboot.backend.apirest.models.dao.IOpcionesPreguntaDao;
import com.tino.springboot.backend.apirest.models.entity.OpcionesPregunta;

@Service
public class OpcionesPreguntaService implements IOpcionesPreguntaService {

	@Autowired
	private IOpcionesPreguntaDao opcionesPreguntaDao;
	
	@Override
	public List<OpcionesPregunta> findAll() {
		return (List<OpcionesPregunta>) opcionesPreguntaDao.findAll();
	}

	@Override
	public OpcionesPregunta findById(Long id) {
		return opcionesPreguntaDao.findById(id).orElse(null);
	}

	@Override
	public OpcionesPregunta save(OpcionesPregunta opcionesPregunta) {
		return opcionesPreguntaDao.save(opcionesPregunta);
	}

	@Override
	public void delete(Long id) {
		opcionesPreguntaDao.deleteById(id);
	}

	@Override
	public List<OpcionesPregunta> findByPreguntaId(Integer preguntaId) {
		  List<OpcionesPregunta> listaToda =	(List<OpcionesPregunta>) opcionesPreguntaDao.findAll();
		  listaToda.removeIf(i -> i.getPreguntaId() != preguntaId);
		  return listaToda;
	}

}
