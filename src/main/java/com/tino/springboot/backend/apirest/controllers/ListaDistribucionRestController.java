package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.ListaDistribucion;
import com.tino.springboot.backend.apirest.models.services.IListaDistribucionService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class ListaDistribucionRestController {
	
	@Autowired
	private IListaDistribucionService listaDistribucionService;
	
	@GetMapping("/listasDistribucion")
	public List<ListaDistribucion> index() {
		return listaDistribucionService.findAll();
	}
	
	@GetMapping("/listasDistribucion/{id}")
	public ListaDistribucion show(@PathVariable Long id) {
		return listaDistribucionService.findById(id);
	}
	
	@GetMapping("/listasDistribucion/usuarios/{id}")
	public List<ListaDistribucion>  showListasUsuario(@PathVariable Integer id) {
		return listaDistribucionService.findByUsuarioId(id);
	}
	
	@PostMapping("/listasDistribucion")
	@ResponseStatus(HttpStatus.CREATED)
	public ListaDistribucion create(@RequestBody ListaDistribucion listaDistribucion) {
		return listaDistribucionService.save(listaDistribucion);
	}
	
	@PutMapping("/listasDistribucion/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ListaDistribucion update(@RequestBody ListaDistribucion listaDistribucion, @PathVariable Long id) {
		
		ListaDistribucion listaDistribucionActual=listaDistribucionService.findById(id);
		
		listaDistribucionActual.setNombreListaDistribucion(listaDistribucion.getNombreListaDistribucion());
		listaDistribucionActual.setUsuarioId(listaDistribucion.getUsuarioId());
		
		return listaDistribucionService.save(listaDistribucionActual);
	}
	
	@DeleteMapping("/listasDistribucion/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		listaDistribucionService.delete(id);
	}

}
