package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.RespuestaOpcion;
import com.tino.springboot.backend.apirest.models.services.IRespuestaOpcionService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class RespuestaOpcionRestController {
	
	@Autowired
	private IRespuestaOpcionService respuestaOpcionService;
	
	
	@GetMapping("/respuestasOpcion")
	public List<RespuestaOpcion> index() {
		return respuestaOpcionService.findAll();
	}
	
	@GetMapping("/respuestasOpcion/{id}")
	public RespuestaOpcion show(@PathVariable Long id) {
		return respuestaOpcionService.findById(id);
	}
	
	@GetMapping("/respuestasOpcion/opcionesPregunta/{id}")
	public List<RespuestaOpcion>  showRespuestasOpcionByOpcionesPreguntaId(@PathVariable Integer id) {
		return respuestaOpcionService.findByOpcionesPreguntaId(id);
	}
	
	@GetMapping("/respuestasOpcion/respuestasPregunta/{id}")
	public List<RespuestaOpcion>  showRespuestasOpcionByRespuestaPreguntaId(@PathVariable Integer id) {
		return respuestaOpcionService.findByRespuestaPreguntaId(id);
	}
	
	@PostMapping("/respuestasOpcion")
	@ResponseStatus(HttpStatus.CREATED)
	public RespuestaOpcion create(@RequestBody RespuestaOpcion respuestaOpcion) {
		return respuestaOpcionService.save(respuestaOpcion);
	}
	
	@PutMapping("/respuestasOpcion/{id}")
	@ResponseStatus(HttpStatus.OK)
	public RespuestaOpcion update(@RequestBody RespuestaOpcion respuestaOpcion, @PathVariable Long id) {
		
		RespuestaOpcion respuestaOpcionActual=respuestaOpcionService.findById(id);
		
		respuestaOpcionActual.setValorRespuestaOpcion(respuestaOpcion.getValorRespuestaOpcion());
		respuestaOpcionActual.setTextoRespuestaOpcion(respuestaOpcion.getTextoRespuestaOpcion());
		
		respuestaOpcionActual.setOpcionesPreguntaId(respuestaOpcion.getOpcionesPreguntaId());
		respuestaOpcionActual.setRespuestaPreguntaId(respuestaOpcion.getRespuestaPreguntaId());
		
		return respuestaOpcionService.save(respuestaOpcionActual);
	}
	
	@DeleteMapping("/respuestasOpcion/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		respuestaOpcionService.delete(id);
	}

}
