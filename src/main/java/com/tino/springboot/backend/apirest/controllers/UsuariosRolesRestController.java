package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.UsuariosRoles;
import com.tino.springboot.backend.apirest.models.services.IUsuariosRolesService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class UsuariosRolesRestController {
	
	@Autowired
	private IUsuariosRolesService usuarioService;
	
	@GetMapping("/usuarioRoles")
	public List<UsuariosRoles> index() {
		return usuarioService.findAll();
	}
	
	@GetMapping("/usuarioRoles/{id}")
	public UsuariosRoles show(@PathVariable Long id) {
		return usuarioService.findById(id);
	}
	
	@PostMapping("/usuarioRoles")
	@ResponseStatus(HttpStatus.CREATED)
	public UsuariosRoles create(@RequestBody UsuariosRoles usuarioRoles) {
		return usuarioService.save(usuarioRoles);
	}
	
	@PutMapping("/usuarioRoles/{id}")
	@ResponseStatus(HttpStatus.OK)
	public UsuariosRoles update(@RequestBody UsuariosRoles usuarioRoles,@PathVariable Long id) {
		
		UsuariosRoles usuarioRolesActual = usuarioService.findById(id);
		
		usuarioRolesActual.setRoleId(usuarioRoles.getRoleId());
		usuarioRolesActual.setUsuarioId(usuarioRoles.getUsuarioId());
		
		return usuarioService.save(usuarioRolesActual);
	}
	
	@DeleteMapping("/usuarioRoles/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		usuarioService.delete(id);
	}
	
	

}
