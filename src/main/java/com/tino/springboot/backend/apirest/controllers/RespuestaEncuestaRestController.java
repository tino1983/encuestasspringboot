package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.RespuestaEncuesta;
import com.tino.springboot.backend.apirest.models.services.IRespuestaEncuestaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class RespuestaEncuestaRestController {

	@Autowired
	private IRespuestaEncuestaService respuestaEncuestaService;
	
	@GetMapping("/respuestasEncuesta")
	public List<RespuestaEncuesta> index() {
		return respuestaEncuestaService.findAll();
	}
	
	@GetMapping("/respuestasEncuesta/{id}")
	public RespuestaEncuesta show(@PathVariable Long id) {
		return respuestaEncuestaService.findById(id);
	}
	
	@GetMapping("/respuestasEncuesta/encuestas/{id}")
	public List<RespuestaEncuesta>  showRespuestasEncuestaByEncuestaId(@PathVariable Integer id) {
		return respuestaEncuestaService.findByEncuestaId(id);
	}
	
	@PostMapping("/respuestasEncuesta")
	@ResponseStatus(HttpStatus.CREATED)
	public RespuestaEncuesta create(@RequestBody RespuestaEncuesta respuestaEncuesta) {
		return respuestaEncuestaService.save(respuestaEncuesta);
	}
	
	@PutMapping("/respuestasEncuesta/{id}")
	@ResponseStatus(HttpStatus.OK)
	public RespuestaEncuesta update(@RequestBody RespuestaEncuesta respuestaEncuesta, @PathVariable Long id) {
		
		RespuestaEncuesta respuestasEncuestaActual=respuestaEncuestaService.findById(id);
		
		respuestasEncuestaActual.setIp(respuestaEncuesta.getIp());
		respuestasEncuestaActual.setMac(respuestaEncuesta.getMac());
		respuestasEncuestaActual.setEsUsuario(respuestaEncuesta.getEsUsuario());
		
		respuestasEncuestaActual.setUsuarioId(respuestaEncuesta.getUsuarioId());
		respuestasEncuestaActual.setEncuestaId(respuestaEncuesta.getEncuestaId());
		
		return respuestaEncuestaService.save(respuestasEncuestaActual);
	}
	
	@DeleteMapping("/respuestasEncuesta/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		respuestaEncuestaService.delete(id);
	}
	
}
