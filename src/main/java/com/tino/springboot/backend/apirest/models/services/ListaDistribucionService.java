package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IListaDistribucionDao;
import com.tino.springboot.backend.apirest.models.entity.ListaDistribucion;

@Service
public class ListaDistribucionService implements IListaDistribucionService {
	
	@Autowired
	private IListaDistribucionDao listaDistribucionDao;

	@Override
	@Transactional(readOnly=true)
	public List<ListaDistribucion> findAll() {
		return (List<ListaDistribucion>) listaDistribucionDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public ListaDistribucion findById(Long id) {
		return listaDistribucionDao.findById(id).orElse(null);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<ListaDistribucion> findByUsuarioId(Integer usuarioId) {
	  List<ListaDistribucion> listaToda =	(List<ListaDistribucion>) listaDistribucionDao.findAll();
	  listaToda.removeIf(i -> (i.getUsuarioId() != usuarioId));
	  return listaToda;
	}

	@Override
	@Transactional
	public ListaDistribucion save(ListaDistribucion listaDistribucion) {
		return listaDistribucionDao.save(listaDistribucion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		listaDistribucionDao.deleteById(id);
	}

}
