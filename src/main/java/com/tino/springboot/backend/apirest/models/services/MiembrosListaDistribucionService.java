package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tino.springboot.backend.apirest.models.dao.IMiembrosListaDistribucionDao;
import com.tino.springboot.backend.apirest.models.entity.MiembrosListaDistribucion;

@Service
public class MiembrosListaDistribucionService implements IMiembrosListaDistribucionService {
	
	@Autowired
	private IMiembrosListaDistribucionDao miembroslistaDistribucionDao;

	@Override
	@Transactional(readOnly=true)
	public List<MiembrosListaDistribucion> findAll() {
		return (List<MiembrosListaDistribucion>) miembroslistaDistribucionDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public MiembrosListaDistribucion findById(Long id) {
		return miembroslistaDistribucionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public MiembrosListaDistribucion save(MiembrosListaDistribucion miembrosListaDistribucion) {
		return miembroslistaDistribucionDao.save(miembrosListaDistribucion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		miembroslistaDistribucionDao.deleteById(id);
	}

	@Override
	@Transactional
	public List<MiembrosListaDistribucion> findByListaDistribucionId(Integer listaDistribucionId) {
		  List<MiembrosListaDistribucion> listaToda =	(List<MiembrosListaDistribucion>) miembroslistaDistribucionDao.findAll();
		  listaToda.removeIf(i -> i.getListaDistribucionId() != listaDistribucionId);
		  return listaToda;
	}

}
