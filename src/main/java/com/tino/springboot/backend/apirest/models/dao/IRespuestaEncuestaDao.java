package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.RespuestaEncuesta;

public interface IRespuestaEncuestaDao extends CrudRepository<RespuestaEncuesta, Long> {

}
