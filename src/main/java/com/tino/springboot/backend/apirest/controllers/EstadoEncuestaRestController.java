package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.EstadoEncuesta;
import com.tino.springboot.backend.apirest.models.services.IEstadoEncuestaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class EstadoEncuestaRestController {
	
	@Autowired
	private IEstadoEncuestaService estadoEncuestaService;
	
	@GetMapping("/estadosEncuesta")
	public List<EstadoEncuesta> index() {
		return estadoEncuestaService.findAll();
	}
	
	@GetMapping("/estadosEncuesta/{id}")
	public EstadoEncuesta show(@PathVariable Long id) {
		return estadoEncuestaService.findById(id);
	}

}
