package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;


@Entity
@Table(name="pregunta")
public class Pregunta  implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String pregunta;
	
	private Double orden;	
	private Boolean exigeRespuesta;
	
	private Integer tipoPreguntaId;
	private Integer encuestaId;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public Long getId() {
		return id;
	}

	public String getPregunta() {
		return pregunta;
	}


	public Double getOrden() {
		return orden;
	}


	public void setOrden(Double orden) {
		this.orden = orden;
	}


	public Boolean getExigeRespuesta() {
		return exigeRespuesta;
	}


	public void setExigeRespuesta(Boolean exigeRespuesta) {
		this.exigeRespuesta = exigeRespuesta;
	}


	public Integer getTipoPreguntaId() {
		return tipoPreguntaId;
	}


	public void setTipoPreguntaId(Integer tipoPreguntaId) {
		this.tipoPreguntaId = tipoPreguntaId;
	}


	public Integer getEncuestaId() {
		return encuestaId;
	}


	public void setEncuestaId(Integer encuestaId) {
		this.encuestaId = encuestaId;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	
}
