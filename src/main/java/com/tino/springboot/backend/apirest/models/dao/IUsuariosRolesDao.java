package com.tino.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tino.springboot.backend.apirest.models.entity.UsuariosRoles;

public interface IUsuariosRolesDao extends CrudRepository<UsuariosRoles,Long> {

}
