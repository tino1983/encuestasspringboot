package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.OpcionesPregunta;
import com.tino.springboot.backend.apirest.models.services.IOpcionesPreguntaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class OpcionesPreguntaRestController {
	
	@Autowired
	private IOpcionesPreguntaService opcionesPreguntaService;
	
	@GetMapping("/opcionesPregunta")
	public List<OpcionesPregunta> index() {
		return opcionesPreguntaService.findAll();
	}
	
	@GetMapping("/opcionesPregunta/{id}")
	public OpcionesPregunta show(@PathVariable Long id) {
		return opcionesPreguntaService.findById(id);
	}
	
	@GetMapping("/opcionesPregunta/preguntas/{id}")
	public List<OpcionesPregunta>  showOpcionesPreguntaByPreguntaId(@PathVariable Integer id) {
		return opcionesPreguntaService.findByPreguntaId(id);
	}
	
	@PostMapping("/opcionesPregunta")
	@ResponseStatus(HttpStatus.CREATED)
	public OpcionesPregunta create(@RequestBody OpcionesPregunta opcionesPregunta) {
		return opcionesPreguntaService.save(opcionesPregunta);
	}
	
	@PutMapping("/opcionesPregunta/{id}")
	@ResponseStatus(HttpStatus.OK)
	public OpcionesPregunta update(@RequestBody OpcionesPregunta opcionesPregunta, @PathVariable Long id) {
		
		OpcionesPregunta opcionesPreguntaActual=opcionesPreguntaService.findById(id);
		
		opcionesPreguntaActual.setTextoOpcion(opcionesPregunta.getTextoOpcion());
		opcionesPreguntaActual.setOrden(opcionesPregunta.getOrden());
		opcionesPreguntaActual.setValorMin(opcionesPregunta.getValorMin());
		opcionesPreguntaActual.setValorMax(opcionesPregunta.getValorMax());
		opcionesPreguntaActual.setStep(opcionesPregunta.getStep());
		
		opcionesPreguntaActual.setPreguntaId(opcionesPregunta.getPreguntaId());
		
		return opcionesPreguntaService.save(opcionesPreguntaActual);
	}
	
	@DeleteMapping("/opcionesPregunta/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		opcionesPreguntaService.delete(id);
	}

}
