package com.tino.springboot.backend.apirest.models.services;

import java.util.List;

import com.tino.springboot.backend.apirest.models.entity.RespuestaPregunta;

public interface IRespuestaPreguntaService {

	public List<RespuestaPregunta> findAll();
	
	public RespuestaPregunta findById(Long id);
	
	public RespuestaPregunta save(RespuestaPregunta respuestaPregunta);
	
	public void delete(Long id);
	

	public List<RespuestaPregunta> findByPreguntaId(Integer preguntaId);
	public List<RespuestaPregunta> findByRespuestaEncuestaId(Integer respuestaEncuestaId);
	
}
